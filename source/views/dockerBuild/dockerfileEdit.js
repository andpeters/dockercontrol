/*
	Author: 	Andreas Peters
	EMail:  	mailbox[@]andreas-peters[dot]net
	Homepage:	www.andreas-peters.net
*/
/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

enyo.kind({
	name: "MyApps.dockerBuildRecordEdit",
	kind: "enyo.Control",
	classes: "onyx",
	clickX: new Array(),
	clickY: new Array(),
	clickDrag: new Array(),
	published: {
		index: "",
		url:   ""
	},
	components:[
		{kind: "onyx.InputDecorator", fit: true, components: [
			{kind: "enyo.RichText", name: "content"}
		]},
		{kind: "onyx.MenuDecorator", components: [
			{kind: "onyx.Button", name:"build", content: "BUILD", classes: "onyx-affirmative"}
		]}
	],

	selectCategoryItem: function(inSender, inEvent) {
		console.log(inEvent);
	},

	selectTagsItem: function(inSender, inEvent) {
		console.log(inEvent);
	},

	/*
		Function:		successGetDockerfile
		Description:	The record was loaded successfull
		Parameters:		inSender = JSON Call object, inReponse = The JSON Repsone
		Return:			none
	*/
	successGetDockerfile: function(inSender, inResponse) {
		if (inResponse) {
			this.$.content.setContent(inResponse);
			this.owner.owner.urlCall("assets/python/dockerbuild.py", {"dockerfile":inResponse, "dockertag": "test"},  enyo.bind(this, "successBuildDockerfile"), enyo.bind(this.owner.owner, "errorDockerBuild"));
		}
	},

	/*
		Function:		successBuildDockerfile
		Description:	The Dockerfile was uploaded successfull
		Parameters:		inSender = JSON Call object, inReponse = The JSON Repsone
		Return:			none
	*/
	successBuildDockerfile: function(inSender, inResponse) {
		if (inResponse) {
			console.log(inResponse);
		}
	},



	rendered: function() {
		this.inherited(arguments);
		this.owner.owner.urlCall(this.url, {}, enyo.bind(this, "successGetDockerfile"), enyo.bind(this.owner.owner, "errorDockerBuild"));
	},

	
});
