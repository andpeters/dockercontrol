enyo.kind({
	name: "MyApps.KCMainView",
	kind: "FittableRows",
	authtoken: "",
	config: {'server': "http://localhost:2369", 'username': "", 'password': ""},
	groups: [
				[{'name': "Containers", 'enyoKind' : "dockerContainer"}],
				[{'name': "Build", 'enyoKind' : "dockerBuild"}],
				[{'name': "Images", 'enyoKind' : "dockerImages"}]
			],
	api: [ ['dockerContainer:=http://localhost:2369'] ],
	components:[
		{kind: "Panels", name: "productPanel", fit: true, components: [
			{kind: "FittableRows", components: [
		       	{kind: "onyx.Toolbar", style:"font-size: 17px;", components: [
			        {content: "dockerControl"}
				]},
				{kind: "enyo.List", name: "menuList", onSetupItem: "setupMenuList", fit:true, components: [
					{name: "menuName"},
					{name: "menuId", style: "visibility:hidden;"}
				]},
		       	{kind: "onyx.Toolbar", style:"height:45px; font-size: 17px;", components: [
					{kind: "onyx.IconButton", src:"assets/enyo-icons-master/spaz_enyo1_icons/icon-settings.png", style:"margin-top: -4px", ondown: "btnClickSettings"} 
				]}
			]}
		]},
		// Konfigurations Dialog
		{kind: "onyx.Popup", name: "settings", floating: true, centered: true, modal: true, scrim:true, components: [
		    {tag: "hr", content: "dockerControl - Login"},
		    {tag: "p", components: [
        		{kind: "onyx.InputDecorator", style: "width:91%", components: [
            		{kind: "onyx.Input", name: "username", placeholder: "Your Username"}
		        ]}
    		]},
    		{kind: "onyx.InputDecorator", style: "width:91%", components: [
        		{kind: "onyx.Input", name: "password", placeholder: "Your Password", type: "password"}
    		]},
    		{tag: "p", components: [
        		{kind: "onyx.Button", ondown: "btnClickSaveSettings", name: "btnSaveSettings", style: "width: 50%;", content: "Save", classes: "onyx-affirmative"},
        		{kind: "onyx.Button", name: "btnCancelSettings", style: "width: 50%;", content: "Cancel", classes: "onyx-negative", style: "margin-left: 10px;"}
    		]}
		]}
	],

	makeAuthToken: function(username, password) {
		var tok = username + ":" + password;
		var hash = base64_encode(tok);
		return "Basic " + hash;
	},
	

	jsonCall: function(method, param, responseFunction, errorFunction) {
		var ajax = new enyo.Ajax({
		    method: "POST",
			url: this.config['server'],
        	headers: { 'Authorization': this.authtoken },
			postBody: {
				"func":method,
				"param":JSON.stringify(param)
			}
    	});
    	ajax.go();
    
		ajax.response(responseFunction);
    	ajax.error(errorFunction);
	},


	rendered: function() {
		this.inherited(arguments);
	
		//Get out the User Credentials
		try {
			this.config['username'] = localStorage.getItem("username");
			this.config['password'] = localStorage.getItem("password");
		} catch (e) {
		}

		// Create Authorisation Token
		this.authtoken = this.makeAuthToken(this.config['username'], this.config['password']);

		// Get All Containers
		//this.jsonCall("/containers/json","", enyo.bind(this,"successGetContainers"), enyo.bind(this,"errorKC"));


		// Create Menu Items 
		this.$.menuList.setCount(this.groups.length);
		this.$.menuList.render();
		this.$.menuList.reflow();

		this.panelCount = 0;
	},

	/*
		Function:		successGetContainers
		Description:	Load all usable Containers
		Parameters:		inSender = Object of Call, inResponse = JSON Object of request
		Return:			none
	*/
	successGetContainers: function(inSender, inResponse) {
		if (inResponse.data) {
			this.containers = inResponse.data;
		}
	},

	/*
		Function:		setupMenuItem
		Description:	Is open from the menuList item as onSetup Event. This function will create the items of the products.
		Parameters:		inSender = Object of MenuList, inEvent = Event of the MenuList
		Return:			none
	*/
	setupMenuList: function(inSender, inEvent) {
		this.$.menuName.setStyle("background:url(assets/"+this.groups[inEvent.index][0].enyoKind+".png) no-repeat; background-position: 50% 50%;");
		if (inSender.isSelected(inEvent.index)) {
			// Create a new panel for the selected app, if its not already exist.
			if (!this.groups[inEvent.index][0].menuId) {
				var i = this.addPanel(this.groups[inEvent.index][0].name,this.groups[inEvent.index][0].enyoKind);
				this.groups[inEvent.index][0].menuId = i;
			} else {
				this.$.productPanel.setIndex(this.groups[inEvent.index][0].menuId);
			}
			// Deselect the choosed item
			this.$.menuList.deselect(inEvent.index);
		}
	},


	/*
		Function:		getAPIFromKind
		Description:	Get out the API Server URL for the Kind
		Parameters:		kindOf = String, name of the products Kind
		Return:			URL = String, API Server URL
	*/
	getAPIFromKind: function(kindOf) {
		try {
			var length = this.api.length;
			for (i = 0 ; i < length; i++) {
				api = this.api[i][0].split(":=");
				if (api[0] === kindOf) {
					return api[1];
				}
			}
		} catch (e) {
		}
	},

	/*
		Function:		btnClickSettings
		Description:	Open the Settings Popup
		Parameters:		Sender and Event of Button Object
		Return:			none
	*/
	btnClickSettings: function(inSender, inEvent) {
		this.$.settings.show();

		this.$.username.setValue(localStorage.getItem('username'));
		this.$.password.setValue(localStorage.getItem('password'));
	},


	/*
		Function:		btnClickSaveSettings
		Description:	Save the user settings into the localStorage
		Parameters:		Sender and Event of Button Object
		Return:			none
	*/
	btnClickSaveSettings: function(inSender, inEvent) {
		localStorage.setItem("username", this.$.username.value);
		localStorage.setItem("password", this.$.password.value);
		this.$.settings.hide();
	},

	errorKC: function(inSender, inResponse) {
		console.log("Error: "+inResponse);
	},

	/*
		Function:		addPanel
		Description:	Create a new panel, with the kind of the chooses app
		Parameters:		name = String as Title, kindOf = String as kind of the app
		Return:			panelIndex as Integer
	*/
	addPanel: function(name, kindOf) {
		var i = this.panelCount++;
		this.$.productPanel.createComponent([{kind: "MyApps."+kindOf}],{owner:this});
		this.$.productPanel.render();
		this.$.productPanel.reflow();
		this.$.productPanel.setIndex(i+1);

		return i+1;
	}
});
