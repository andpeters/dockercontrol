#!/usr/bin/env python

import cgi
import docker
import io
import cgitb

cgitb.enable()

print("\n\n")


param = cgi.FieldStorage();

dockerFile = param.getvalue('dockerfile')
dockerTag  = param.getvalue('dockertag')

cli = docker.Client(base_url="http://localhost:2369")

f = io.BytesIO(dockerFile.encode('utf-8'))
response = [line for line in cli.build(
    fileobj=f, rm=True, tag=dockerTag
)]

print(response)

