<?php

global $REX;

$avAuth = new OOavEnterAuth($REX);

$login = $avAuth->checkAuth();

if (!$login) {
	exit;
}

if (!$REX['COM_USER']->getValue('login')) {
	exit;
}	

$func   = htmlentities(rex_request("func","string",""));
$param = json_decode(stripcslashes(rex_request("param", "string")), true);

$ispUsername = 'redaxo';
$ispPassword = 'em6a5#"S>Tqz';
$soapLocation = 'https://kis.aventer.biz:1443/remote/index.php';
$soapBilling = 'https://kis.aventer.biz:1443/remote/billing.php';
$soapUri = 'https://kis.aventer.biz:1443/remote/';


switch ($func) {
	case "getUser": getUser(); break;
	case "getGroups": getGroups(); break;
	case "getAPI": getAPI(); break;
	case "getInvoicesOfClient": getInvoicesOfClient(); break;
	case "getOutAllDNS": getOutAllDNS(); break;
	case "createUpdateOpenID": createUpdateOpenID(); break;
	case "checkGroupMembership": checkGroupMembership($param); break;
	case "getKinds": getKinds(); break;
	case "getMembersareaMenu": getMembersareaMenu(); break;
	case "printOutInvoice": printOutInvoice($param); break;
}

function getKinds() {
	global $REX;
	$sqlRef = new rex_sql();
	$sqlRef->setQuery(sprintf("select rex_com_group from %s where login = '%s' limit 1","rex_com_user", $REX['COM_USER']->getValue('login')));
	$groupArray = $sqlRef->getArray();

	$groupList = explode(',',$groupArray[0]['rex_com_group']);	

	foreach ($groupList as $group) {
		$sqlGroupRef = new rex_sql();
		$sqlGroupRef->setQuery(sprintf("select * from %s where id = '%s' and enyokind is not null limit 1","rex_com_group", $group));	
		$tmp = $sqlGroupRef->getArray();		
		if (count($tmp[0])) {
			$groups[] = $tmp;
		}
	}
    $res['method'] = "getKinds";
	$res['data'] = $groups;		
	echo json_encode($res);		
}


/*
	Function:       getUser
	Description:    Get out all Userinformations
	Parameters:
	Return:         JSON Array of User informations
*/   		
function getUser() {
	global $REX;
	$avAuth = new OOavEnterAuth($REX);
	$user = $avAuth->getUser();
    $res['method'] = "getUser";
	$res['data'] = $user;		
	echo json_encode($res);		
}


/*
	Function:       getGroups
	Description:    Get out all Groups of the current user
	Parameters:
	Return:         JSON Array of all Groups
*/   		
function getGroups() {
	global $REX;
	$avAuth = new OOavEnterAuth($REX);
	$groups = $avAuth->getGroups();
	$res['method'] = "getGroups";
	$res['data'] = $groups;
	echo json_encode($res);
}

function getAPI() {
	global $REX;
	$sqlRef = new rex_sql();
	$sqlRef->setQuery(sprintf("select api from %s where login = '%s' limit 1","rex_com_user", $REX['COM_USER']->getValue('login')));
    $res['method'] = "getAPI";
	$res['data'] = $sqlRef->getArray();		
	echo json_encode($res);		
}

function getInvoicesOfClient() {
	global $REX;
	$ispconfig = new OOIspConfig($REX);
	$data = $ispconfig->getInvoicesOfClient();
	$res['method'] = "getInvoicesOfClient";	
	$res['data'] = $data;
	echo json_encode($res);		
}	

function getOutAllDNS() {
	global $REX;

    $dns = new OOCallbackDNS();
    $dns->setClient();
    $domains = $dns->getAllDomains();

    $res['method'] = "getOutAllDNS";

	$y = 0;
    foreach ($domains as $i) {
        $records = $dns->getAllRecordsOfZone($i['id']);
		$res['data'][$y] = $i;
		$res['data'][$y] = $records;
		$y++;
	}
    
	echo json_encode($res);		
}


/*
	Function:		createUpdateOpenID
	Description:	Create or Update the OpenID Identity
	Parameters:		
	Return:
*/
function createUpdateOpenID() {
	global $REX;
	
	// Check if the User is allowed to use this function
	$avAuth = new OOavEnterAuth($REX);
	$group[group] = "OpenID";
	if (!$avAuth->checkGroupMembership($group))
		return;

	$openid = new OOOpenID($REX);
    $res['method'] = "createUpdateOpenID";
	$res['data'] = "";		
	echo json_encode($res);		
}

/*
	Function:		checkGroupMembership
	Description:	Check if the current User is Member of a give group
	Parameters:		$param = JSON Array
						'group' = The to checkd group
	Return:			JSON Array
*/
function checkGroupMembership($param) {
	global $REX;
	$avAuth = new OOavEnterAuth($REX);
	
	if (!isset($param['group'])) {
		return;
	}	
	
	$data = $avAuth->checkGroupMembership($param);	
    $res['method'] = "checkGroupMembership";
	$res['data'] = $data;		
	echo json_encode($res);		
}
  			
/*
	Function:
	Description:
	Parameters:
	Return:
*/
function getMembersareaMenu() {
    $res['method'] = "getMembersareaMenu";
    $res['data'][0]['name'] = "Account Information";
    $res['data'][0]['id'] = 1;
    $res['data'][1]['name'] = "Change Passwort";
    $res['data'][1]['id'] = 2;
    $res['data'][2]['name'] = "All Invoices";
    $res['data'][2]['id'] = 3;
    $res['data'][3]['name'] = "Cancel Contract";
    $res['data'][3]['id'] = 4;
    $res['data'][4]['name'] = "Privacy Information";
    echo json_encode($res);
}

  	
/*
	Function:		printOutInvoice
	Description:	print out the given invoice as pdf
	Parameters:		$param
						'id' = invoice id
	Return:			PDF File
*/
function printOutInvoice($param) {
	global $REX;
	$ispconfig = new OOIspConfig($REX);

	if (!isset($param['id']))
		return;

	echo $ispconfig->printOutInvoice($param['id']);
}
		

